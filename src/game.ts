

import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)



const building = new Entity()
building.setParent(scene)
const gltfShape_2 = new GLTFShape('models/model1/model1.glb')
building.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(7, 0, 6),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
building.addComponentOrReplace(transform_3)
engine.addEntity(building)


/*const girafee = new Entity()
girafee.setParent(scene)
const gltfShape_3 = new GLTFShape('models/giraffe/giraffe2.glb')
girafee.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  //position: new Vector3(10.5, 0, 14.5),
  //rotation: new Quaternion(0, 0, 0, 1),
  position: new Vector3(3.5,0,14.5),
  rotation: new Quaternion.Euler(0, -90, 0),
  scale: new Vector3(0.03, 0.03, 0.03)
})
girafee.addComponentOrReplace(transform_4)
engine.addEntity(girafee)*/

let girafeeShape = new GLTFShape('models/giraffe/giraffe2.glb')
let girafee = spawnGltfX(girafeeShape, 3.5,0,14.5,    0, -90, 0,        0.03,0.03,0.03)


///////// Example of adding the BuilderHUD to your scene and attaching it to one existing scene entity.
//const hud:BuilderHUD =  new BuilderHUD()
//hud.attachToEntity(girafee)
//hud.attachToEntity(existingEntity2)
//hud.setDefaultParent(scene)

// Quaternion.Euler(0,0,0)

/*0,0:  --------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(3.5,0,14.5,  0,-90,0,  0.03,0.03,0.03)
preview.js:8 0,0:  -------------------------------------------------*/